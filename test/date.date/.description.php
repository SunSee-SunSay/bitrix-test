<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
	"NAME" => GetMessage("DATE_VALUE"),
	"DESCRIPTION" => GetMessage("DATE_DESC"),
	"PATH" => array(
		"ID" => "dv_components",
		"NAME" => GetMessage("DATE_PROJECT_COMPONENTS"),
	),
);
?>