<?
// файл /bitrix/php_interface/init.php
// регистрируем обработчик
AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", Array("MyClass", "OnAfterIBlockSectionUpdateHandler"));

class MyClass
{
    // создаем обработчик события "OnAfterIBlockSectionUpdate"
    function OnAfterIBlockSectionUpdateHandler(&$arFields)
    {
        if($arFields["UF_ARCHIVE"] == 1 && $arFields["IBLOCK_ID"] == 6) {
            $bs = new CIBlockSection;
            $date = date('Y-m-d H:i:s');
            $arFieldsNew = $arFields;
            //переопределяем значения
            $arFieldsNew["NAME"] = $arFields['NAME'].' '.$date;
            $arFieldsNew["IBLOCK_ID"] = '7';
            //создаем новый раздел, получаем его ID
            $ID = $bs->Add($arFieldsNew);
            //обращаемся к элементам копируемого раздела
            $arSelect = Array("*");
            $arFilter = Array("IBLOCK_ID"=>'6', "SECTION_ID" => $arFields['ID']);
            //получили список
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            //поэлементно
            while ($ob = $res->GetNextElement())
            {
                $arFieldsEl = $ob->GetFields(); //массив полей
                $arProps = $ob->GetProperties(); //массив свойств
                $PROP['MARK'] = $arProps['MARK']['VALUE'];
                $PROP['MODEL'] = $arProps['MODEL']['VALUE'];
                $PROP['PHOTO'] = $arProps['PHOTO']['VALUE'];
                //создаем массив полей элемента
                $arElNew = array(
                    'NAME' => $arFieldsEl['NAME'].' архивная копия от '.date('Y-m-d'),
                    'IBLOCK_ID' => '7',
                    'IBLOCK_SECTION_ID' => $ID,
                    'PROPERTY_VALUES' =>  $PROP,
                    'ACTIVE' => $arFieldsEl['ACTIVE'],
                    'PREVIEW_PICTURE' => $arFieldsEl['PREVIEW_PICTURE'],
                    'PREVIEW_TEXT' => $arFieldsEl['PREVIEW_TEXT'],
                    'DETAIL_PICTURE' => $arFieldsEl['DETAIL_PICTURE'],
                    'DETAIL_TEXT' => $arFieldsEl['DETAIL_TEXT'],
                );
                $el = new CIBlockElement;
                //создаем сам элемент
                $NewEl = $el->Add($arElNew);
            }
        }
    }
}
?>